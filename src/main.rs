/*
 * Cross platform package manager
 * 
 * This package manager acts as shim for apt, pacman and MacPorts. It might be 
 * useless to you but I am maximal annoyed by the difference of these 
 * package managers (especially pacman).
 * 
 * Target package managers:
 * - apt (debian/WSL)
 * - pacman (arch/msys2)
 * - port (Macos, [bsd?])
 * 
 * Some systems might even have multiple package managers like windows (pacman in msys2, 
 * apt in WSL, Find-Package (Etc...)) which is most annoying.
 * 
 * You may define override the search of a package manager by defining a fully qualified
 * path in the environment variable PKG_EXE.
 * 
 * Main operations:
 * - search
 * - list (installed / version)
 * - files (list installed files)
 * - install
 * - uninstall
 * - update (update metadata) -> might use a better name?
 * - upgrade (update all installed packages)
 * 
 * This is all I need.
 */

// includes
use std::env;
use std::path::PathBuf;

/**
 * This is a runtime setting
 */
#[derive(Debug)]
enum PkgEnv {
    UNKNOWN,
    APT,
    PACMAN,
    PORT
}

fn env_is_set(v: &str) -> bool {
    match env::var(v) {
        Ok(_s) => true,
        _ => false
    }
}

fn main() {
    let mut is_posix = true;
    let _pkg_exec = ["apt", "pacman", "port"];

    /*
     * The first thing to do is find out in which environment we are running. 
     * This could be a compile time setting, however, at least on windows,
     * there might be multiple package managers available depending on 
     * environmen.
     * 
     * https://doc.rust-lang.org/reference/conditional-compilation.html#target_os
     * 
     * in general we need to know if we are on a posix-ish system (not cmd and not 
     * powershell). We assume, that this is true for al lsystems but windows. On 
     * windows we probe for mingw, cygwin and msys.
     */
    if env::consts::OS == "windows" {
        if env_is_set("MSYSTEM") == false && 
            env_is_set("CYGWIN") == false && 
            env_is_set("MINGW_PREFIX") == false {
            is_posix = false;
        }
    }

    // let _is_mingw: bool = env_is_set("MSYSTEM");
    // println!("{}", std::path::MAIN_SEPARATOR); // Print the path separator -> \\ in mingw
    println!("System: {}", env::consts::OS);           // Prints the current OS.
    println!("POSIX: {}", is_posix);

    /*
     * if the environment variable PKG_EXE is set and available, we'll use this executable.
     * if not present, we'll abort.
     */
    let mut pkg_exe = PathBuf::new();
    match env::var("PKG_EXE") {
        Ok(_val) => {
            pkg_exe = PathBuf::from(_val);
            if !pkg_exe.exists() {
                println!("Packagemanager defined in PKG_EXE does not exists {}", pkg_exe.display());
                std::process::exit(1);
            }
        },
        Err(_e) => {} // do nothing, println!("couldn't interpret {}: {}", "PKG_EXE", e)
    }

    /*
     * now probe for the package manager
     * 
     * We do this at runtime, because on any system there could be mone than one installed.
     * The first one found will be used.
     * 
     */
    //let mut exe: std::path::PathBuf = std::path::PathBuf::new();
    if pkg_exe.exists() == false {
        //println!("Finding in PATH");
        match env::var_os("PATH") {
            Some(paths) => {
                'outer: for path in env::split_paths(&paths) {
                    //println!("----> {}", path.display());
                    let mut f = path;
                    for e in &_pkg_exec {
                        if env::consts::OS == "windows" {
                            // this works on windows but not linux
                            f.set_file_name(e); 
                            f.set_extension("exe");
                        } else {
                            // this works on linux
                            f.push(e); 
                        }
                        if f.exists() {
                            pkg_exe = f; // std::path::PathBuf::from(f);
                            //println!("path: {} {}", exe.exists(), exe.display());
                            break 'outer;
                        }
                    }
                }
            }
            None => {
                println!("PATH is not defined in the environment.");
                std::process::exit(3);
            }
        }
    }

    // check if we found something
    if !pkg_exe.as_os_str().is_empty() {
        println!("path: {} {}", pkg_exe.exists(), pkg_exe.display());
    } else {
        println!("Failed to find package manager, make sure it is in your $PATH.");
        std::process::exit(1);
    }

    // figure out which package manager we are using
    // without .exe on windows
    let pkg_exe_name: String = pkg_exe.file_stem().unwrap().to_str().unwrap().into();

    let pkg_type: PkgEnv = if pkg_exe_name == "pacman" {
        PkgEnv::PACMAN
    } else if pkg_exe_name == "apt" {
        PkgEnv::APT
    } else if pkg_exe_name == "port" {
        PkgEnv::PORT
    } else {
        PkgEnv::UNKNOWN
    };

    println!("Manager: {} ({:?})", pkg_exe_name, pkg_type);

}
